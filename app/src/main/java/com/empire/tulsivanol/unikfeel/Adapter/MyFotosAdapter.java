package com.empire.tulsivanol.unikfeel.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Fragments.PostDetailFragment;
import com.empire.tulsivanol.unikfeel.Model.Post;
import com.empire.tulsivanol.unikfeel.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class MyFotosAdapter extends RecyclerView.Adapter<MyFotosAdapter.ImageViewHolder> {

    private Context mContext;
    private List<Post> mPosts;

    public MyFotosAdapter(Context context, List<Post> posts){
        mContext = context;
        mPosts = posts;
    }

    @NonNull
    @Override
    public MyFotosAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fotos_item, parent, false);
        return new MyFotosAdapter.ImageViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onBindViewHolder(@NonNull final MyFotosAdapter.ImageViewHolder holder, final int position) {

        final Post post = mPosts.get(position);

        if (post.getPosttype().equals("image")) {
            holder.post_image.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(post.getPostimage())
                    .placeholder(R.drawable.music_icon)
                    .into(holder.post_image);
        } else if (post.getPosttype().equals("video")) {
            holder.post_video.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(post.getPostimage())
                    .placeholder(R.drawable.video_icon)
                    .into(holder.post_video);
        } else if (post.getPosttype().equals("audio")) {
            holder.post_audio.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(R.drawable.music_icon)
                    .into(holder.post_audio);
        } else if (post.getPosttype().equals("blog")) {
            holder.postBlogLayout.setVisibility(View.VISIBLE);
            holder.post_blog.setText(Html.fromHtml(post.getDescription()));
        }

        Glide.with(mContext).load(post.getPostimage()).into(holder.post_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = mContext.getSharedPreferences("PREFS", MODE_PRIVATE).edit();
                editor.putString("postid", post.getPostid());
                editor.apply();

                ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PostDetailFragment()).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public ImageView post_image, post_audio, post_video;
        private RelativeLayout postBlogLayout;
        private TextView post_blog;


        public ImageViewHolder(View itemView) {
            super(itemView);

            post_image = itemView.findViewById(R.id.post_image);
            post_audio = itemView.findViewById(R.id.user_post_audio);
            post_video = itemView.findViewById(R.id.user_post_video);
            postBlogLayout = itemView.findViewById(R.id.user_post_blog_layout);
            post_blog = itemView.findViewById(R.id.user_post_blog);

        }
    }
}