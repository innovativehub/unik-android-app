package com.empire.tulsivanol.unikfeel.activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.R;
import com.empire.tulsivanol.unikfeel.music.FFMpegHelper;
import com.empire.tulsivanol.unikfeel.music.Music;
import com.empire.tulsivanol.unikfeel.music.MusicAdapter;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class AudioMergingActivity extends AppCompatActivity implements MusicAdapter.MusicClickListener {

    private RecyclerView recyclerView;
    private List<Music> musicList;
    private MusicAdapter musicAdapter;
    private MediaPlayer mediaPlayer;
    private String mAudioUri = "", mMusicPath = "";
    private ImageView mUploadAudioBtn, mStopMusicButton;
    private FFmpeg fFmpeg;
    private FFMpegHelper helper;
    private TextView mBackBtn;
    private static final String TAG = "AudioMergingActivity";
    private String downloadedFilePath = "";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_merging);

        fFmpeg = FFmpeg.getInstance(AudioMergingActivity.this);
        helper = new FFMpegHelper(fFmpeg, this,downloadedFilePath);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle("Fetching");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog.create();
        }

        mUploadAudioBtn = findViewById(R.id.upload_music);
        mStopMusicButton = findViewById(R.id.stop_music);
        mBackBtn = findViewById(R.id.backBtn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        startActivity(new Intent(AudioMergingActivity.this, PostAudioVideoActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(AudioMergingActivity.this, PostAudioVideoActivity.class));
                        finish();
                    }
                } else {
                    startActivity(new Intent(AudioMergingActivity.this, PostAudioVideoActivity.class));
                    finish();
                }
            }
        });

        Log.d(TAG, "onCreate: uri " + getIntent().getStringExtra("tulsivanol"));

        if (getIntent().hasExtra("tulsivanol")) {
            mAudioUri = getIntent().getStringExtra("tulsivanol");
        }

        recyclerView = findViewById(R.id.music_lists);
        getMusic();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        musicAdapter = new MusicAdapter(this, musicList, this);
        recyclerView.setAdapter(musicAdapter);

        mStopMusicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                }
                mStopMusicButton.setVisibility(View.GONE);
            }
        });

        mUploadAudioBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (mediaPlayer == null) {
                    Log.d(TAG, "onClick: " + mMusicPath);
                    if (mAudioUri.equals("")) {
                        Toast.makeText(AudioMergingActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (mMusicPath.equals("")) {
                        Toast.makeText(AudioMergingActivity.this, "Please choose a music", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String audioPath = getPath(AudioMergingActivity.this, Uri.parse(mAudioUri));
                    downloadFile(mAudioUri);
                    if (downloadedFilePath.equals("")){
                        Toast.makeText(AudioMergingActivity.this, "Please wait", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    addBackgroundAudioCommand(audioPath, downloadedFilePath);
                } else {
                    Toast.makeText(AudioMergingActivity.this, "Pause the music first", Toast.LENGTH_SHORT).show();
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                    return;
                }
            }
        });
    }

    private void getMusic() {
        musicList = new ArrayList<>();
        musicList.add(new Music("", "Music 1", "http://docs.google.com/uc?id=1PG_pzU_bnyPX0tkTMF46DxFonR8J4jhM"));
        musicList.add(new Music("", "Music 2", "https://docs.google.com/uc?id=1Tu1aGak4qoOHM8QUDmLQkZgW4PefP16O"));
        musicList.add(new Music("", "Music 3", "https://docs.google.com/uc?id=1pSrataNhGhTU2uJOSXDHDsYPZsWslqex"));
        musicList.add(new Music("", "Music 4", "https://docs.google.com/uc?id=1jCKIVFXE7C16hd5Pot3jQIxdXQ7WQqS5"));
        musicList.add(new Music("", "Music 5", "https://docs.google.com/uc?id=1YUhGbxJPAYy9l7YO4oqgiVLkBkUY7e4h"));
        musicList.add(new Music("", "Music 6", "https://docs.google.com/uc?id=17kXO3JslbkR7p2dlnebqZeFZoYQO8O5M"));
        musicList.add(new Music("", "Music 7", "https://docs.google.com/uc?id=1ewUcKg4AY3NCBqoEEqjLiv7YJkYX1hmO"));
        musicList.add(new Music("", "Music 8", "https://docs.google.com/uc?id=1G0EsG-wD7R5YHGqTrEtYSLZ6lK5r2y06"));
        musicList.add(new Music("", "Music 9", "https://docs.google.com/uc?id=1Kd1B6hPRbRgnXepmRD8-sBW0ggr5TTEz"));
        musicList.add(new Music("", "Music 10", "https://docs.google.com/uc?id=1_818mvnHNckxNAOC6gibP3n4u5khKw5g"));
        musicList.add(new Music("", "Music 11", "https://docs.google.com/uc?id=1SB6Z3VsU879HwhbCQW2bOu39SP-XNiuV"));
        musicList.add(new Music("", "Music 12", "https://docs.google.com/uc?id=1F8ddfERYsyXGMspNeR4exZoaNL3U4XBu"));
        musicList.add(new Music("", "Music 13", "https://docs.google.com/uc?id=1bxPcc0v9ST69Tvxzl9iiwrEQHt8KL4ea"));
        musicList.add(new Music("", "Music 14", "https://docs.google.com/uc?id=1_mIbxKXc640LaJPWQI16VK0x7EMpvJQW"));
        musicList.add(new Music("", "Music 15", "https://docs.google.com/uc?id=1UNiACSD13O5_CqYJHcd2h2lpN-qdzrQB"));
        musicList.add(new Music("", "Music 16", "https://docs.google.com/uc?id=1fCG9P0ZMCz84lgHIOKiyaL7dk_aZwrvb"));
        musicList.add(new Music("", "Music 17", "https://docs.google.com/uc?id=1ELdwykjATGjUCx4N2VO3dgLoMxg_BZN3"));
        musicList.add(new Music("", "Music 18", "https://docs.google.com/uc?id=1y3TvYFeOfQvDSDBEeO8NdNPzMZD1n5nZ"));
        musicList.add(new Music("", "Music 19", "https://docs.google.com/uc?id=1A7SfI4A6YiiVpdwDh7A2z2jHewYbvy4b"));
        musicList.add(new Music("", "Music 20", "https://docs.google.com/uc?id=1b56sA2jnMrmxWSTZRR9m5jkCSDgmZkMu"));
        musicList.add(new Music("", "Music 21", "https://docs.google.com/uc?id=1LjbDd_1nss5aeC_qaaLHIlsrZMUN1xN6"));
        musicList.add(new Music("", "Music 22", "https://docs.google.com/uc?id=1EICVqjVOMf4-2pWloNYvhUAMhC4kOShg"));
        musicList.add(new Music("", "Music 23", "https://docs.google.com/uc?id=1vadaj7YaGE_DjDMpqsTE7J0ncaAzdAXo"));
        musicList.add(new Music("", "Music 24", "https://docs.google.com/uc?id=1Fy0_CK_zNUJfZ-Uo_6dzNcl3XjOSiWHo"));
        }

    @Override
    public void onMusicClickListener(final Music music) {
        Toast.makeText(this, music.getMusicUrl(), Toast.LENGTH_SHORT).show();
        mMusicPath = music.getMusicUrl();
        try {
            mediaPlayer = new MediaPlayer();
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.reset();
            }
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(AudioMergingActivity.this, Uri.parse(music.getMusicUrl()));
            progressDialog.show();
            mediaPlayer.prepare();
            mediaPlayer.start();
            progressDialog.hide();
            if (mediaPlayer.isPlaying()) {
                mStopMusicButton.setVisibility(View.VISIBLE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addBackgroundAudioCommand(String audioPath, String bgAudio) {
        File moviesDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/UnikFeel/music");
        String filePrefix = "beautifyAudio";
        String fileExtn = ".mp3";
        if (moviesDir.exists()) {
            System.out.println("Directory exists");
        } else {
            moviesDir.mkdir();
        }

        File dest = new File(moviesDir, filePrefix + "%03d" + System.currentTimeMillis() + fileExtn);

        //test
//        String[] command = {"-i",audioPath,"-i",bgAudio,"-filter_complex", "[0:a]volume=0.99[a1];[1:a]volume=0.3[a2];[a1][a2]amerge=inputs=2,volume=1.3,pan=stereo|c0<c0+c2|c1<c1+c3[out]","-map","[out]","-strict","-2",dest.getAbsolutePath()};
        //endtest


//        ffmpeg -i bgmusic.mp3 -i audio.mp3 -filter_complex "[1:a]asplit=2[sc][mix];[0:a][sc]sidechaincompress=threshold=0.003:ratio=20[bg]; [bg][mix]amerge[final]" -map [final] final.mp3
//        ffmpeg -y -i audio1.mp3 -i audio2.mp3 -filter_complex "[0:0]volume=0.09[a];[1:0]volume=1.8[b];[a][b]amix=inputs=2:duration=longest" -c:a libmp3lame output.mp3
//        String[] bgAudioCommand = {"-i", bgAudio, "-i", audioPath, "-filter_complex", "[1:a]asplit=2[sc][mix];[0:a][sc]sidechaincompress=threshold=0.003:ratio=20[bg]; [bg][mix]amerge[]","-map", dest.getAbsolutePath()};
        String[] mixAudioCommand = {"-i", audioPath, "-i",bgAudio , "-filter_complex", "[0:0]volume=0.09[a];[1:0]volume=1.8[b];[a][b]amix=inputs=2:duration=first", "-c:a","libmp3lame", dest.getAbsolutePath()};
//        String[] complexCommand = {"-i",audioPath, "-i", bgAudio, "-filter_complex", "amix=inputs=2:duration=first:dropout_transition=2", dest.getAbsolutePath()};
        helper.execFFmpegBinary(mixAudioCommand);
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.release();
            mediaPlayer.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.release();
            mediaPlayer.stop();
        }
    }

    public boolean downloadFile(final String path)
    {
        try
        {
            URL url = new URL(path);

            URLConnection ucon = url.openConnection();
            ucon.setReadTimeout(5000);
            ucon.setConnectTimeout(10000);

            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

            File file = new File(this.getDir("UnikFeel", Context.MODE_PRIVATE) + "/bg_audio.mp3");

            if (file.exists())
            {
                file.delete();
            }
            file.createNewFile();

            Log.d(TAG, "downloadFile: "+file.getAbsolutePath());

            downloadedFilePath = file.getAbsolutePath();

            FileOutputStream outStream = new FileOutputStream(file);
            byte[] buff = new byte[5 * 1024];

            int len;
            while ((len = inStream.read(buff)) != -1)
            {
                outStream.write(buff, 0, len);
            }

            outStream.flush();
            outStream.close();
            inStream.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}