package com.empire.tulsivanol.unikfeel.api;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class OreoNotification extends ContextWrapper {
    private static final String CHANNEL_ID = "com.empire.tulsivanol.unikfeel";
    private static final String CHANNEL_NAME = "unikfeel";
    private NotificationManager notificationManager;

    public OreoNotification(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannel() {
        @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel(
                CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
        );
        channel.enableLights(true);
        channel.enableVibration(true);

        getManager().createNotificationChannel(channel);

    }

    public NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getOreoNotification(String title, String body, PendingIntent pendingIntent,
                                                    Uri soundUri, String icon) {

        return new Notification.Builder(
                getApplicationContext(),
                CHANNEL_ID
        ).setContentText(body)
                .setContentTitle(title)
                .setSound(soundUri)
                .setSmallIcon(Integer.parseInt(icon))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

    }
}
