package com.empire.tulsivanol.unikfeel.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.R;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {

    TextInputEditText username, fullname, email, password, mobileNumber, referralCode, confirmPassword;
    Button register;
    TextView txt_login, termsDialog;
    MaterialCheckBox mCheckBox;


    FirebaseAuth auth;
    DatabaseReference reference;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        imageView = findViewById(R.id.app_logo_register);
        imageView.setImageResource(R.mipmap.logo);

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        fullname = findViewById(R.id.fullname);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirm_password);
        referralCode = findViewById(R.id.referral_Code);
        mobileNumber = findViewById(R.id.mobile_number);
        register = findViewById(R.id.register);
        txt_login = findViewById(R.id.txt_login);
        termsDialog = findViewById(R.id.terms_and_conditions);
        mCheckBox = findViewById(R.id.agreement);

        termsDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View customLayout = getLayoutInflater().inflate(R.layout.terms_and_condition, null);
                final AlertDialog.Builder dialog = new AlertDialog.Builder(RegisterActivity.this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.setView(customLayout);
                }
                InputStream stream = getResources().openRawResource(R.raw.terms_and_conditions);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                int i;
                try {
                    i = stream.read();
                    while (i != -1) {
                        byteArrayOutputStream.write(i);
                        i = stream.read();
                    }
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                TextView termsView = customLayout.findViewById(R.id.terms_view);
                ImageView close = customLayout.findViewById(R.id.close);
                termsView.setText(byteArrayOutputStream.toString());
                dialog.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });

                final AlertDialog alertDialog = dialog.create();
                alertDialog.show();
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });
            }
        });

        auth = FirebaseAuth.getInstance();

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_username = username.getText().toString();
                String str_fullname = fullname.getText().toString();
                String str_email = email.getText().toString();
                String str_password = password.getText().toString();
                String confirm_password = confirmPassword.getText().toString();
                String mobile_number = mobileNumber.getText().toString();
                String referral_code = referralCode.getText().toString();

                if (str_username.equals("")) {
                    username.setError("Username required");
                    return;
                }
                if (str_fullname.equals("")) {
                    fullname.setError("Full name required");
                    return;
                }
                if (str_email.equals("")) {
                    email.setError("Email required");
                    return;
                }
                if (str_password.equals("")) {
                    password.setError("Password required");
                    return;
                }
                if (mobile_number.equals("")) {
                    mobileNumber.setError("Mobile Number required");
                    return;
                }

                if (!str_password.equals(confirm_password)) {
                    password.setError("Password must match");
                    Toast.makeText(RegisterActivity.this, "Password must match", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mCheckBox.isChecked()) {
                    register(str_username, str_fullname, str_email, str_password, mobile_number, referral_code);
                } else {
                    Toast.makeText(RegisterActivity.this, "Please accept our terms and conditions", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public void register(final String username, final String fullname, String email, String password, final String mobile_number, final String referral_code) {
        Intent in = new Intent(RegisterActivity.this, PhoneVerifyActivity.class);
        in.putExtra("username", username.toLowerCase());
        in.putExtra("fullname", fullname);
        in.putExtra("email", email);
        in.putExtra("password", password);
        in.putExtra("mobile_number", mobile_number);
        in.putExtra("referral_code", referral_code);
        startActivity(in);
    }
}
