package com.empire.tulsivanol.unikfeel.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Adapter.MyFotosAdapter;
import com.empire.tulsivanol.unikfeel.Model.Cover;
import com.empire.tulsivanol.unikfeel.Model.Post;
import com.empire.tulsivanol.unikfeel.Model.User;
import com.empire.tulsivanol.unikfeel.R;
import com.empire.tulsivanol.unikfeel.activities.EditProfileActivity;
import com.empire.tulsivanol.unikfeel.activities.FollowersActivity;
import com.empire.tulsivanol.unikfeel.activities.OptionsActivity;
import com.empire.tulsivanol.unikfeel.activities.PostActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {

    ImageView image_profile, options, uploadCoverPic;
    TextView posts, followers, following, fullname, bio, username, link, private_notice;
    Button edit_profile;
    private LinearLayout mFollowers, mFollowings;

    private DatabaseReference mRootReference;

    private List<String> mySaves;

    FirebaseUser firebaseUser;
    String profileid, searchedUserId, user;

    private RecyclerView recyclerView;
    private MyFotosAdapter myFotosAdapter;
    private List<Post> postList;

    private RecyclerView recyclerView_list;
    private MyFotosAdapter myFotosAdapter_saves;
    private List<Post> postList_saves;

    ImageButton my_fotos, saved_fotos;
    ImageView mCoverPic;

    private static final String TAG = "ProfileFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mRootReference = FirebaseDatabase.getInstance().getReference("Follow");

        SharedPreferences prefs = getContext().getSharedPreferences("PREFS", MODE_PRIVATE);
        profileid = prefs.getString("profileid", "none");
        user = prefs.getString("type", "none");
//        Log.d(TAG, "onCreateView: userId" + profileid + "\nCurrentUserId" + firebaseUser.getUid());

        image_profile = view.findViewById(R.id.image_profile);
        uploadCoverPic = view.findViewById(R.id.upload_cover_pic);
        private_notice = view.findViewById(R.id.private_notice);
        mCoverPic = view.findViewById(R.id.user_cover_pic);
        posts = view.findViewById(R.id.posts);
        followers = view.findViewById(R.id.followers);
        following = view.findViewById(R.id.following);
        fullname = view.findViewById(R.id.fullname);
        bio = view.findViewById(R.id.bio);
        link = view.findViewById(R.id.link);
        edit_profile = view.findViewById(R.id.edit_profile);
        username = view.findViewById(R.id.username);
        my_fotos = view.findViewById(R.id.my_fotos);
        saved_fotos = view.findViewById(R.id.saved_fotos);
        options = view.findViewById(R.id.options);

        mFollowers = view.findViewById(R.id.view_followers);
        mFollowings = view.findViewById(R.id.view_following);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        postList = new ArrayList<>();
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myFotosAdapter = new MyFotosAdapter(getContext(), postList);
        recyclerView.setAdapter(myFotosAdapter);

        recyclerView_list = view.findViewById(R.id.recycler_view_list);
        recyclerView_list.setHasFixedSize(true);
        LinearLayoutManager mLayoutManagers = new LinearLayoutManager(getContext());
        recyclerView_list.setLayoutManager(mLayoutManagers);
        postList_saves = new ArrayList<>();
        myFotosAdapter_saves = new MyFotosAdapter(getContext(), postList);
        recyclerView_list.setAdapter(myFotosAdapter);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView_list.setVisibility(View.GONE);

        userInfo(container.getContext());
        getFollowers();
        getNrPosts();
        myFotos();
        mySaves();

        if (firebaseUser.getUid().equals(profileid)) {
            Log.d("PMSBHJHJSH", "onCreateView: " + firebaseUser.getEmail());
            edit_profile.setText("Edit Profile");
        } else {
            checkFollow();
        }

        uploadCoverPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), PostActivity.class));
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String btn = edit_profile.getText().toString();

                if (btn.equals("Edit Profile")) {

                    startActivity(new Intent(getContext(), EditProfileActivity.class));

                } else if (btn.equals("follow")) {

                    mRootReference.child(firebaseUser.getUid())
                            // following
                            .child("following").
                            //this user (user.getId)
                                    child(profileid).setValue(false);


                    mRootReference.child(profileid)
                            .child("followers").child(firebaseUser.getUid()).setValue(false);

                    edit_profile.setText("requested");
                    addNotification();
                } else if (btn.equals("following") || btn.equals("requested")) {

                    mRootReference.child(firebaseUser.getUid())
                            .child("following").child(profileid).removeValue();
                    mRootReference.child(profileid)
                            .child("followers").child(firebaseUser.getUid()).removeValue();

                }
            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), OptionsActivity.class));
            }
        });

        my_fotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView_list.setVisibility(View.GONE);
            }
        });

        saved_fotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setVisibility(View.GONE);
                recyclerView_list.setVisibility(View.VISIBLE);
            }
        });


        mFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FollowersActivity.class);
                intent.putExtra("id", profileid);
                intent.putExtra("title", "followers");
                startActivity(intent);
            }
        });

        mFollowings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FollowersActivity.class);
                intent.putExtra("id", profileid);
                intent.putExtra("title", "following");
                startActivity(intent);
            }
        });

        return view;
    }

    private void addNotification() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(profileid);

        String notificationId = reference.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("userid", firebaseUser.getUid());
        hashMap.put("notificationId", notificationId);
        hashMap.put("text", "wants to follow you");
        hashMap.put("postid", "");
        hashMap.put("ispost", false);

        reference.push().setValue(hashMap);
    }

    private void userInfo(final Context context) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(profileid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (getContext() == null) {
                    return;
                }
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {

                    FirebaseDatabase.getInstance().getReference("UserCoverPic")
                            .child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                Cover cover = dataSnapshot.getValue(Cover.class);
                                Glide.with(context)
                                        .load(cover.getCoverPicUrl())
                                        .placeholder(R.color.colorPrimary)
                                        .into(mCoverPic);
                            } else {
                                Glide.with(context)
                                        .load("https://www.setaswall.com/wp-content/uploads/2017/06/Beutiful-Thought-FB-Cove-Pic-850-x-315.jpg")
                                        .placeholder(R.color.colorPrimary)
                                        .into(mCoverPic);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    Glide.with(getContext()).load(user.getImageurl()).into(image_profile);
                    username.setText(user.getUsername());
                    fullname.setText(user.getFullname());
                    bio.setText(user.getBio());
                    link.setText(user.getLink());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void checkFollow() {
        DatabaseReference reference = mRootReference.child(firebaseUser.getUid()).child("following");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (dataSnapshot.child(profileid).exists()) {
                        if (dataSnapshot.child(profileid).getValue().equals(true)) {
                            edit_profile.setText("following");
                            saved_fotos.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            my_fotos.setVisibility(View.VISIBLE);
                            private_notice.setVisibility(View.GONE);
                        } else if (dataSnapshot.child(profileid).getValue().equals(false)) {
                            edit_profile.setText("requested");
                            saved_fotos.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                            my_fotos.setVisibility(View.GONE);
                            private_notice.setVisibility(View.VISIBLE);
                        } else if (dataSnapshot.child(profileid).getKey().equals(profileid)) {
                            Log.d("MATCHENOTFOUND", "onDataChange: " + dataSnapshot.child(profileid).getKey() + "\n \t" + profileid);
                            edit_profile.setText("follow");
                            saved_fotos.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                            my_fotos.setVisibility(View.GONE);
                            private_notice.setVisibility(View.VISIBLE);
                        }
                    } else {
                        edit_profile.setText("follow");
                        saved_fotos.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        my_fotos.setVisibility(View.GONE);
                        private_notice.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.d("NOTEXISTS", "onDataChange: " + dataSnapshot.getValue());
                    edit_profile.setText("follow");
                    saved_fotos.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    my_fotos.setVisibility(View.GONE);
                    private_notice.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getFollowers() {
        DatabaseReference reference = mRootReference.child(profileid).child("followers");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d("Followers", "onDataChange: " + ds.getValue());
                    if (ds.getValue().equals(true)) {
                        i += 1;
                    }
                }
                followers.setText("" + i);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference reference1 = mRootReference.child(profileid).child("following");
        reference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d("Following", "onDataChange: " + ds.getValue());
                    if (ds.getValue().equals(true)) {
                        i += 1;
                    }
                }
                following.setText("" + i);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getNrPosts() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Post post = snapshot.getValue(Post.class);
                    if (post.getPublisher().equals(profileid)) {
                        i++;
                    }
                }
                posts.setText("" + i);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void myFotos() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Post post = snapshot.getValue(Post.class);
                    if (post.getPublisher().equals(profileid)) {
                        postList.add(post);
                    }
                }
                Collections.reverse(postList);
                myFotosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void mySaves() {
        mySaves = new ArrayList<>();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Saves").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    mySaves.add(snapshot.getKey());
                }
                readSaves();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void readSaves() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postList_saves.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Post post = snapshot.getValue(Post.class);

                    for (String id : mySaves) {
                        if (post.getPostid().equals(id)) {
                            postList_saves.add(post);
                        }
                    }
                }
                myFotosAdapter_saves.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
