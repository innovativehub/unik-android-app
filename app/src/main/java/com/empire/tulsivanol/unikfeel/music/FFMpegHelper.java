package com.empire.tulsivanol.unikfeel.music;

import android.app.ProgressDialog;
import android.app.usage.NetworkStats;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.activities.MainActivity;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;

public class FFMpegHelper {
    private FFmpeg fFmpeg;
    private Context context;

    private static final String TAG = "FFMpegHelper";
    private String filePath;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabase;

    public FFMpegHelper(FFmpeg fFmpeg, Context context,String filePath) {
        this.context = context;
        this.fFmpeg = fFmpeg;
        this.filePath = filePath;

        mDatabase = FirebaseDatabase.getInstance().getReference("Posts");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    private ProgressDialog progressDialog;


    public void execFFmpegBinary(final String[] command) {
        Log.d(TAG, "onFailure: failed" + filePath);
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage("Uploading audio");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                progressDialog.create();
            }
            progressDialog.show();;
            fFmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    progressDialog.hide();
                }

                @Override
                public void onSuccess() {
                    try {
                        fFmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                            @Override
                            public void onFailure(String message) {
                                super.onFailure(message);
                                progressDialog.hide();
                                Log.d(TAG, "onFailure: failed" + message);
                            }

                            @Override
                            public void onSuccess(String message) {
                                super.onSuccess(message);
                                progressDialog.hide();
                                // TODO: 7/29/2020 Start Activity
                            }

                            @Override
                            public void onProgress(String message) {
                                super.onProgress(message);
                                progressDialog.hide();
                                Log.d(TAG, "onProgress: " + message);
                            }

                            @Override
                            public void onStart() {
                                super.onStart();
                                Log.d(TAG, "onStart: starting process");
                            }

                            @Override
                            public void onFinish() {
                                super.onFinish();
                                Log.d(TAG, "onFinish: finished");
                                progressDialog.show();
                                uploadAudioToFirebase();
                                progressDialog.hide();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {
                    progressDialog.hide();
                }
            });

        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
            Log.d(TAG, "execFFmpegBinary: ", e.getCause());
        }
    }

    private void uploadAudioToFirebase() {
        final Uri audioUri = Uri.fromFile(new File(filePath));
                Log.d("AUDIOURI", "onActivityResult: " + audioUri);
                final StorageReference storageReference = FirebaseStorage.getInstance().getReference("audios");
                storageReference.child(System.currentTimeMillis() + currentUser.getUid())
                        .putFile(audioUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()) {
                                        Uri downloadUri = task.getResult();
                                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                        Date date = new Date();
                                        String dateString = dateFormat.format(date).toString();
                                        String postId = mDatabase.push().getKey();
                                        HashMap<String, Object> hashMap = new HashMap<>();
                                        hashMap.put("postid", postId);
                                        hashMap.put("postimage", downloadUri.toString());
                                        hashMap.put("posttype", "audio");
                                        hashMap.put("postedDate", dateString);
                                        hashMap.put("description", audioUri.getLastPathSegment());
                                        hashMap.put("publisher", currentUser.getUid());
                                        mDatabase.child(postId).setValue(hashMap);

                                        Toast.makeText(context, "Audio Posted successfully", Toast.LENGTH_SHORT).show();
                                        context.startActivity(new Intent(context, MainActivity.class).
                                                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    } else {
                                        Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
