package com.empire.tulsivanol.unikfeel.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Model.CaptionImage;
import com.empire.tulsivanol.unikfeel.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CaptionImageAdapter extends RecyclerView.Adapter<CaptionImageAdapter.CaptionImageViewHolder> {

    private Context context;
    private List<CaptionImage> imageList;
    private OnImageClickListener imageClickListener;

    public CaptionImageAdapter(Context context, List<CaptionImage> imageList, OnImageClickListener imageClickListener) {
        this.context = context;
        this.imageList = imageList;
        this.imageClickListener = imageClickListener;
    }

    @NonNull
    @Override
    public CaptionImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image_background, parent, false);
        return new CaptionImageViewHolder(view,imageClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CaptionImageViewHolder holder, int position) {
        CaptionImage image = imageList.get(position);
        holder.setBackgroundImage(image);
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class CaptionImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mBackgroundImage;
        OnImageClickListener listener;

        public CaptionImageViewHolder(@NonNull View itemView,OnImageClickListener listener) {
            super(itemView);
            this.listener = listener;
            mBackgroundImage = itemView.findViewById(R.id.caption_background_image);

            itemView.setOnClickListener(this);
        }

        private void setBackgroundImage(CaptionImage image) {
            Glide.with(context)
                    .load(image.getCaptionImage())
                    .placeholder(R.drawable.placeholder)
                    .into(mBackgroundImage);
        }

        @Override
        public void onClick(View view) {
            listener.onImageClick(imageList.get(getAdapterPosition()),getAdapterPosition());
        }
    }

    public interface OnImageClickListener{
        void onImageClick(CaptionImage image,int position);
    }
}
