package com.empire.tulsivanol.unikfeel.Model;

public class Cover {
    private String coverPicUrl;
    private String coverPicId;

    public Cover() {
    }

    public Cover(String coverPicUrl, String coverPicId) {
        this.coverPicUrl = coverPicUrl;
        this.coverPicId = coverPicId;
    }

    public String getCoverPicUrl() {
        return coverPicUrl;
    }

    public void setCoverPicUrl(String coverPicUrl) {
        this.coverPicUrl = coverPicUrl;
    }

    public String getCoverPicId() {
        return coverPicId;
    }

    public void setCoverPicId(String coverPicId) {
        this.coverPicId = coverPicId;
    }
}
