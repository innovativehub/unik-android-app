package com.empire.tulsivanol.unikfeel.Model;

public class CaptionImage {

    private String captionImage;

    public CaptionImage(String captionImage) {
        this.captionImage = captionImage;
    }

    public String getCaptionImage() {
        return captionImage;
    }

    public void setCaptionImage(String captionImage) {
        this.captionImage = captionImage;
    }
}
