package com.empire.tulsivanol.unikfeel.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Model.User;
import com.empire.tulsivanol.unikfeel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.empire.tulsivanol.unikfeel.activities.CaptionDesignActivity.INTENT_ACTION_IMAGE;
import static com.empire.tulsivanol.unikfeel.activities.CaptionDesignActivity.INTENT_ACTION_TEXT;

public class AddImagePostActivity extends AppCompatActivity {

    private FirebaseUser firebaseUser;
    private DatabaseReference mDatabase;
    private StorageReference storageReference;
    private CircleImageView mUserImage;
    private ImageView mPostImage, mCloseView, mPost;
    private TextInputEditText mPostCaption;
    private String postImageUrl = null;
    private String postCaption = null;
    private String userImageUrl = null;

    private RadioGroup dialogRadioGroup;
    private MaterialButton mSubmitBtn;

    private static final String TAG = "AddImagePostActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image_post);

        mUserImage = findViewById(R.id.post_user_image);
        mPostImage = findViewById(R.id.post_fresh_image);
        mPostCaption = findViewById(R.id.post_caption_text);
        mCloseView = findViewById(R.id.close_caption);
        mPost = findViewById(R.id.postImage);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference("Posts");
        storageReference = FirebaseStorage.getInstance().getReference("drafts");

        postImageUrl = getIntent().getStringExtra(INTENT_ACTION_IMAGE);
        postCaption = getIntent().getStringExtra(INTENT_ACTION_TEXT);

        mCloseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = getLayoutInflater().inflate(R.layout.dialog_close, null);
                mSubmitBtn = dialogView.findViewById(R.id.dialog_submit);
                AlertDialog.Builder builder = new AlertDialog.Builder(AddImagePostActivity.this);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                dialogRadioGroup = dialogView.findViewById(R.id.radioGroup);
                alertDialog.show();
                dialogRadioGroup.clearCheck();
                mSubmitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RadioButton radioButton = dialogRadioGroup.findViewById(dialogRadioGroup.getCheckedRadioButtonId());
                        if (radioButton.getText().equals("Discard")) {
                            startActivity(new Intent(AddImagePostActivity.this, MainActivity.class));
                            finish();
                        } else if (radioButton.getText().equals("Save as Draft")) {
                            final ProgressDialog pd = new ProgressDialog(AddImagePostActivity.this);
                            pd.setCanceledOnTouchOutside(false);
                            pd.setMessage("Saving as draft");
                            pd.show();
                            if (postImageUrl == null || postCaption == null) {
                                Log.d(TAG, "onClick: failed");
                                return;
                            }
                            storageReference.child(System.currentTimeMillis() + firebaseUser.getUid())
                                    .putFile(Uri.fromFile(new File(postImageUrl)))
                                    .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Uri> task) {
                                                        if (task.isSuccessful()) {
                                                            String downloadUri = task.getResult().toString();
                                                            DatabaseReference draftReference = FirebaseDatabase.getInstance().getReference("Drafts");
                                                            String draftId = mDatabase.push().getKey();

                                                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                            Date date = new Date();
                                                            String dateString = dateFormat.format(date).toString();

                                                            HashMap<String, Object> hashMap = new HashMap<>();
                                                            hashMap.put("postid", draftId);
                                                            hashMap.put("postimage", downloadUri);
                                                            hashMap.put("posttype", "draft");
                                                            hashMap.put("description", mPostCaption.getText().toString());
                                                            hashMap.put("publisher", firebaseUser.getUid());
                                                            hashMap.put("postedDate", dateString);
                                                            FirebaseDatabase.getInstance().getReference("Drafts").child(draftId).setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        pd.dismiss();
                                                                        Toast.makeText(AddImagePostActivity.this, "Draft saved successfully", Toast.LENGTH_SHORT).show();
                                                                        startActivity(new Intent(AddImagePostActivity.this, MainActivity.class));
                                                                        finish();
                                                                        alertDialog.cancel();
                                                                    } else {
                                                                        pd.dismiss();
                                                                        alertDialog.cancel();
                                                                        Toast.makeText(AddImagePostActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });

                                                        } else {
                                                            pd.dismiss();
                                                            alertDialog.cancel();
                                                            Toast.makeText(AddImagePostActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            } else {
                                                pd.dismiss();
                                                alertDialog.cancel();
                                                Toast.makeText(AddImagePostActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        } else {
                            return;
                        }
                    }
                });
            }
        });

        loadUserImage();

        Glide.with(this)
                .load(postImageUrl)
                .into(mPostImage);
        mPostCaption.setText("#" + postCaption);

        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog pd = new ProgressDialog(AddImagePostActivity.this);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Posting");
                pd.show();
                StorageReference reference = FirebaseStorage.getInstance().getReference("images");
                reference.child(System.currentTimeMillis()
                        + firebaseUser.getUid())
                        .putFile(Uri.fromFile(new File(postImageUrl)))
                        .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                if (task.isSuccessful()) {
                                    task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if (task.isSuccessful()) {
                                                String downloadUri = task.getResult().toString();
                                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                Date date = new Date();
                                                String dateString = dateFormat.format(date).toString();
                                                String postId = mDatabase.push().getKey();
                                                HashMap<String, Object> hashMap = new HashMap<>();
                                                hashMap.put("postid", postId);
                                                hashMap.put("postimage", downloadUri);
                                                hashMap.put("posttype", "image");
                                                hashMap.put("description", mPostCaption.getText().toString());
                                                hashMap.put("publisher", firebaseUser.getUid());
                                                hashMap.put("postedDate", dateString);
                                                mDatabase.child(postId).setValue(hashMap);
                                                pd.dismiss();
                                                Toast.makeText(AddImagePostActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                                File file = new File(postImageUrl);
                                                if (file.exists()) {
                                                    if (file.delete()) {
                                                        Log.d(TAG, "onComplete: deleted");
                                                    } else {
                                                        Log.d(TAG, "onComplete: not deleted");
                                                    }
                                                }
                                                startActivity(new Intent(AddImagePostActivity.this, MainActivity.class));
                                                finish();

                                            } else {
                                                pd.dismiss();
                                                Toast.makeText(AddImagePostActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    pd.dismiss();
                                    Toast.makeText(AddImagePostActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

    }

    private void loadUserImage() {
        DatabaseReference userDB = FirebaseDatabase.getInstance().getReference("Users");
        userDB.child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    userImageUrl = user.getImageurl();
                    Glide.with(AddImagePostActivity.this)
                            .load(userImageUrl)
                            .into(mUserImage);
                } else {
                    Log.d(TAG, "onDataChange: ");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
        User user = new User();
        Log.d(TAG, "loadUserImage: " + user.getImageurl());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}