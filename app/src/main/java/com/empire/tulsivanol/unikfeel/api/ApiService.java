package com.empire.tulsivanol.unikfeel.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {

    @Headers(
            {"Content-Type:application/json",
                    "Authorization:key=AAAAMzOhYwM:APA91bFrQDa2bjlhj6axIRektT6dbTNTsLUQUooBxzO3g1uqsnymDgEjEcS55GF26Is4JHo-wN-CB86JEzIO6neN82Kqit7Q4vc5wantfRPyUEHMbYNe5a64CqCsU1ulMVy61xyURPDW"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
