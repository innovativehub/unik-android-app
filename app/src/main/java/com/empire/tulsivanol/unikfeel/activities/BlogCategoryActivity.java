package com.empire.tulsivanol.unikfeel.activities;

import android.os.Bundle;

import com.empire.tulsivanol.unikfeel.Adapter.BlogAdapter;
import com.empire.tulsivanol.unikfeel.Model.Blog;
import com.empire.tulsivanol.unikfeel.R;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BlogCategoryActivity extends AppCompatActivity {

    private List<Blog> blogs;
    private RecyclerView recyclerView;
    private BlogAdapter blogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_category);

        setTitle("Blog Category");

        recyclerView = findViewById(R.id.blog_list);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        blogs = new ArrayList<>();
        Blog blog = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Spiritual");
        blogs.add(blog);
        Blog blog1 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Health");
        blogs.add(blog1);
        Blog blog2 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Education");
        blogs.add(blog2);
        Blog blog3 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Culture");
        blogs.add(blog3);
        Blog blog4 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Wildlife");
        blogs.add(blog4);
        Blog blog5 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Psychology");
        blogs.add(blog5);
        Blog blog6 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Religion");
        blogs.add(blog6);
        Blog blog7 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Politics");
        blogs.add(blog7);
        Blog blog8 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Energy");
        blogs.add(blog8);
        Blog blog9 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Economics");
        blogs.add(blog9);
        Blog blog10 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "Science And Tech");
        blogs.add(blog10);
        Blog blog11 = new Blog("https://images.unsplash.com/photo-1555020653-d7c1a90a3951?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80", "History");
        blogs.add(blog11);

        blogAdapter = new BlogAdapter(this, blogs);
        recyclerView.setAdapter(blogAdapter);
    }
}