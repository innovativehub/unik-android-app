package com.empire.tulsivanol.unikfeel.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.empire.tulsivanol.unikfeel.R
import com.empire.tulsivanol.unikfeel.music.Music
import com.empire.tulsivanol.unikfeel.music.MusicAdapter
import com.empire.tulsivanol.unikfeel.video.AudioVideoMerger
import com.empire.tulsivanol.unikfeel.video.FFMpegCallback
import com.empire.tulsivanol.unikfeel.video.OutputType
import com.empire.tulsivanol.unikfeel.video.TextOnVideo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.video.trimmer.interfaces.OnTrimVideoListener
import com.video.trimmer.interfaces.OnVideoListener
import kotlinx.android.synthetic.main.activity_video_editing.*
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class VideoEditingActivity : AppCompatActivity(), OnTrimVideoListener, OnVideoListener, MusicAdapter.MusicClickListener, FFMpegCallback {
    lateinit var downloadedFilePath: File
    private lateinit var filePath: File
    private val musicList = listOf(
            Music("", "Music 1", "http://docs.google.com/uc?id=1PG_pzU_bnyPX0tkTMF46DxFonR8J4jhM"),
            Music("", "Music 2", "https://docs.google.com/uc?id=1Tu1aGak4qoOHM8QUDmLQkZgW4PefP16O"),
            Music("", "Music 3", "https://docs.google.com/uc?id=1pSrataNhGhTU2uJOSXDHDsYPZsWslqex"),
            Music("", "Music 4", "https://docs.google.com/uc?id=1jCKIVFXE7C16hd5Pot3jQIxdXQ7WQqS5"),
            Music("", "Music 5", "https://docs.google.com/uc?id=1YUhGbxJPAYy9l7YO4oqgiVLkBkUY7e4h"),
            Music("", "Music 6", "https://docs.google.com/uc?id=17kXO3JslbkR7p2dlnebqZeFZoYQO8O5M"),
            Music("", "Music 7", "https://docs.google.com/uc?id=1ewUcKg4AY3NCBqoEEqjLiv7YJkYX1hmO"),
            Music("", "Music 8", "https://docs.google.com/uc?id=1G0EsG-wD7R5YHGqTrEtYSLZ6lK5r2y06"),
            Music("", "Music 9", "https://docs.google.com/uc?id=1Kd1B6hPRbRgnXepmRD8-sBW0ggr5TTEz"),
            Music("", "Music 10", "https://docs.google.com/uc?id=1_818mvnHNckxNAOC6gibP3n4u5khKw5g"),
            Music("", "Music 11", "https://docs.google.com/uc?id=1SB6Z3VsU879HwhbCQW2bOu39SP-XNiuV"),
            Music("", "Music 12", "https://docs.google.com/uc?id=1F8ddfERYsyXGMspNeR4exZoaNL3U4XBu"),
            Music("", "Music 13", "https://docs.google.com/uc?id=1bxPcc0v9ST69Tvxzl9iiwrEQHt8KL4ea"),
            Music("", "Music 14", "https://docs.google.com/uc?id=1_mIbxKXc640LaJPWQI16VK0x7EMpvJQW"),
            Music("", "Music 15", "https://docs.google.com/uc?id=1UNiACSD13O5_CqYJHcd2h2lpN-qdzrQB"),
            Music("", "Music 16", "https://docs.google.com/uc?id=1fCG9P0ZMCz84lgHIOKiyaL7dk_aZwrvb"),
            Music("", "Music 17", "https://docs.google.com/uc?id=1ELdwykjATGjUCx4N2VO3dgLoMxg_BZN3"),
            Music("", "Music 18", "https://docs.google.com/uc?id=1y3TvYFeOfQvDSDBEeO8NdNPzMZD1n5nZ"),
            Music("", "Music 19", "https://docs.google.com/uc?id=1A7SfI4A6YiiVpdwDh7A2z2jHewYbvy4b"),
            Music("", "Music 20", "https://docs.google.com/uc?id=1b56sA2jnMrmxWSTZRR9m5jkCSDgmZkMu"),
            Music("", "Music 21", "https://docs.google.com/uc?id=1LjbDd_1nss5aeC_qaaLHIlsrZMUN1xN6"),
            Music("", "Music 22", "https://docs.google.com/uc?id=1EICVqjVOMf4-2pWloNYvhUAMhC4kOShg"),
            Music("", "Music 23", "https://docs.google.com/uc?id=1vadaj7YaGE_DjDMpqsTE7J0ncaAzdAXo"),
            Music("", "Music 24", "https://docs.google.com/uc?id=1Fy0_CK_zNUJfZ-Uo_6dzNcl3XjOSiWHo")
    )

    private val TAG = "VideoEditingActivity"

    lateinit var videoFile: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_editing)

        filePath = File(getDir("UnikFeel", Context.MODE_PRIVATE).toString() + "/trimVideo/")
        if (filePath.exists()) {
            filePath.delete()
        } else {
            filePath.mkdir()
        }

        video_music_list.apply {
            layoutManager = LinearLayoutManager(this@VideoEditingActivity)
            adapter = MusicAdapter(this@VideoEditingActivity, musicList, this@VideoEditingActivity)
        }

        videoTrimmer.setTextTimeSelectionTypeface(Typeface.DEFAULT)
                .setOnTrimVideoListener(this)
                .setOnVideoListener(this)
                .setVideoURI(Uri.parse(intent.getStringExtra("videoname")))
                .setVideoInformationVisibility(true)
                .setMaxDuration(10)
                .setMinDuration(2)
                .setDestinationPath(filePath!!.absolutePath)

        back.setOnClickListener {
            videoTrimmer.onCancelClicked()
            startActivity(Intent(this@VideoEditingActivity, PostAudioVideoActivity::class.java))
            finish()
            finishAffinity()
        }
        if (save.text.equals("Save")) {
            save.setOnClickListener {
                videoTrimmer.onSaveClicked()
                Toast.makeText(this@VideoEditingActivity, "Choose a music if you want a background music", Toast.LENGTH_LONG).show()
                save.text = "Upload"
            }
        }

        if (save.text.equals("Upload")) {
            save.setOnClickListener {
                if (downloadedFilePath.equals("")) {
                    uploadVideoOnly()
                } else {
                    AudioVideoMerger.with(applicationContext!!)
                            .setAudioFile(downloadedFilePath)
                            .setVideoFile(videoFile)
                            .setOutputPath(filePath.absolutePath)
                            .setOutputFileName("merged_" + System.currentTimeMillis() + ".mp4")
                            .setCallback(this@VideoEditingActivity)
                            .merge()
                }
            }
        }

    }

    private fun uploadVideoOnly() {
        val pd = ProgressDialog(this)
        pd.setCanceledOnTouchOutside(false)
        pd.setMessage("Posting")
        pd.show()
        if (videoFile == null) {
            Log.d("PostAudioVideoActivity", "onActivityResult: empty video")
            return
        }
        var currentUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser;
        var mDatabase: DatabaseReference = FirebaseDatabase.getInstance().getReference("Posts")
        val storageReference = FirebaseStorage.getInstance().getReference("videos")
        storageReference.child(System.currentTimeMillis().toString() + currentUser?.getUid()).putFile(Uri.fromFile(videoFile)).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result.storage.downloadUrl.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result.toString()
                        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                        val date = Date()
                        val dateString = dateFormat.format(date).toString()
                        val postId: String = mDatabase.push().getKey().toString()
                        val hashMap = HashMap<String, Any?>()
                        hashMap["postid"] = postId
                        hashMap["postimage"] = downloadUri
                        hashMap["posttype"] = "video"
                        hashMap["description"] = task.result.lastPathSegment
                        hashMap["publisher"] = currentUser?.getUid()
                        hashMap["postedDate"] = dateString
                        mDatabase.child(postId).setValue(hashMap)
                        Toast.makeText(this@VideoEditingActivity, "Video Posted successfully", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@VideoEditingActivity, MainActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this@VideoEditingActivity, task.exception!!.message, Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(this@VideoEditingActivity, task.exception!!.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun cancelAction() {
        Log.d(TAG, "cancelAction: ")
    }

    override fun getResult(uri: Uri) {
        videoFile = File(uri.toString())
    }

    override fun onError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }

    override fun onTrimStarted() {
        Log.d(TAG, "onTrimStarted: ")
        save.text = "Save"
    }

    override fun onVideoPrepared() {
        Log.d(TAG, "onVideoPrepared: ")
    }

    override fun onMusicClickListener(music: Music?) {
        Log.d(TAG, "onMusicClickListener: " + music?.musicUrl)
        downloadFile(music?.musicUrl)
    }


    fun downloadFile(path: String?): Boolean {
        try {
            val url = URL(path)
            val ucon = url.openConnection()
            ucon.readTimeout = 5000
            ucon.connectTimeout = 10000
            val `is` = ucon.getInputStream()
            val inStream = BufferedInputStream(`is`, 1024 * 5)
            val file = File(getDir("UnikFeel", Context.MODE_PRIVATE).toString() + "/bg_audio.mp3")
            if (file.exists()) {
                file.delete()
            }
            file.createNewFile()
            Log.d("AudioMergingActivity", "downloadFile: " + file.absolutePath)
            downloadedFilePath = file
            val outStream = FileOutputStream(file)
            val buff = ByteArray(5 * 1024)
            var len: Int
            while (inStream.read(buff).also { len = it } != -1) {
                outStream.write(buff, 0, len)
            }
            outStream.flush()
            outStream.close()
            inStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    override fun onProgress(progress: String) {
        Log.d(TAG, "onProgress: ")
    }

    override fun onSuccess(convertedFile: File, type: String) {
        Log.d(TAG, "onSuccess: " + convertedFile)
        when {
            type.equals(OutputType.TYPE_VIDEO) -> {
                upload(convertedFile);
            }
            type.equals(OutputType.TYPE_AUDIO) -> {
                uploadVideoWithAudio(convertedFile)
            }
            type.equals(OutputType.TYPE_GIF) -> {

            }
        }

    }

    private fun upload(convertedFile: File) {
        val pd = ProgressDialog(this)
        pd.setCanceledOnTouchOutside(false)
        pd.setMessage("Posting")
        pd.show()
        if (convertedFile == null) {
            Log.d("PostAudioVideoActivity", "onActivityResult: empty video")
            return
        }
        var currentUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser;
        var mDatabase: DatabaseReference = FirebaseDatabase.getInstance().getReference("Posts")
        val storageReference = FirebaseStorage.getInstance().getReference("videos")
        storageReference.child(System.currentTimeMillis().toString() + currentUser?.getUid()).putFile(Uri.fromFile(convertedFile)).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result.storage.downloadUrl.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result.toString()
                        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                        val date = Date()
                        val dateString = dateFormat.format(date).toString()
                        val postId: String = mDatabase.push().getKey().toString()
                        val hashMap = HashMap<String, Any?>()
                        hashMap["postid"] = postId
                        hashMap["postimage"] = downloadUri
                        hashMap["posttype"] = "video"
                        hashMap["description"] = task.result.lastPathSegment
                        hashMap["publisher"] = currentUser?.getUid()
                        hashMap["postedDate"] = dateString
                        mDatabase.child(postId).setValue(hashMap)
                        Toast.makeText(this@VideoEditingActivity, "Video Posted successfully", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@VideoEditingActivity, MainActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this@VideoEditingActivity, task.exception!!.message, Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(this@VideoEditingActivity, task.exception!!.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun uploadVideoWithAudio(inputFile: File) {
        var name: String? = FirebaseAuth.getInstance().currentUser?.displayName
        TextOnVideo.with(this!!)
                .setFile(inputFile) //Video File
                .setOutputPath(filePath.absolutePath)
                .setOutputFileName("textOnVideo_" + System.currentTimeMillis() + ".mp4")
                .setText("@${name}") //Text to be displayed
                .setColor("#000000") //Color of Text
                .setSize("25") //Size of text //This will add background with border on text
                .setPosition(TextOnVideo.POSITION_BOTTOM_RIGHT) //Can be selected
                .setCallback(this@VideoEditingActivity)
                .draw()
    }

    override fun onFailure(error: Exception) {
        Log.d(TAG, "onFailure: ")
    }

    override fun onNotAvailable(error: Exception) {
        Log.d(TAG, "onNotAvailable: ")
    }

    override fun onFinish() {
        Log.d(TAG, "onFinish: ")
    }

}