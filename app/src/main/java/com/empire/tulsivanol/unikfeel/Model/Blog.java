package com.empire.tulsivanol.unikfeel.Model;

public class Blog {

    private String backgroundImage, blogCategory;

    public Blog() {
    }

    public Blog(String backgroundImage, String blogCategory) {
        this.backgroundImage = backgroundImage;
        this.blogCategory = blogCategory;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getBlogCategory() {
        return blogCategory;
    }

    public void setBlogCategory(String blogCategory) {
        this.blogCategory = blogCategory;
    }
}
