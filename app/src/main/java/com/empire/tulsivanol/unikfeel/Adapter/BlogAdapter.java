package com.empire.tulsivanol.unikfeel.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Model.Blog;
import com.empire.tulsivanol.unikfeel.R;
import com.empire.tulsivanol.unikfeel.activities.BlogActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.BlogViewHolder> {

    private Context context;
    private List<Blog> blogList;

    public BlogAdapter(Context context, List<Blog> blogList) {
        this.context = context;
        this.blogList = blogList;
    }

    @NonNull
    @Override
    public BlogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_blog, parent, false);
        return new BlogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlogViewHolder holder, int position) {
        final Blog blog = blogList.get(position);
        holder.bindViews(blog);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BlogActivity.class);
                intent.putExtra("title", blog.getBlogCategory());
                context.startActivity(intent);
//                Toast.makeText(context, "clicked" + blog.getBlogCategory(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogList.size();
    }

    class BlogViewHolder extends RecyclerView.ViewHolder {
        private ImageView mBlogBackground;
        private TextView mBlogCategoryName;

        public BlogViewHolder(@NonNull View itemView) {
            super(itemView);
            mBlogBackground = itemView.findViewById(R.id.blog_background_image);
            mBlogCategoryName = itemView.findViewById(R.id.blog_category_name);

        }

        private void bindViews(Blog blog) {

            mBlogCategoryName.setText(blog.getBlogCategory());

            Glide.with(context)
                    .load(blog.getBackgroundImage())
                    .placeholder(R.drawable.placeholder)
                    .into(mBlogBackground);
        }
    }
}
