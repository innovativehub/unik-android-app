package com.empire.tulsivanol.unikfeel.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Adapter.CaptionImageAdapter;
import com.empire.tulsivanol.unikfeel.Model.CaptionImage;
import com.empire.tulsivanol.unikfeel.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.SaveSettings;

import static com.empire.tulsivanol.unikfeel.activities.PostAudioVideoActivity.INTENT_CAPTION;

public class CaptionDesignActivity extends AppCompatActivity implements CaptionImageAdapter.OnImageClickListener {

    private static final int PICK_IMAGE = 12;
    private static final int SAVE_IMAGE_REQUEST_CODE = 13;
    private CaptionImageAdapter adapter;
    private RecyclerView mBackgroundImages;
    private List<CaptionImage> imageList = new ArrayList<>();
    private PhotoEditorView mEditorView;
    private PhotoEditor mPhotoEditor;

    private ImageView mNextPage;
    private Spinner mFontSpinner;

    private RelativeLayout mTextEditingView;
    private TextView mWallpaper, mText;

    private String fileName = null;
    private static final String TAG = "CaptionDesignActivity";
    public static final String INTENT_ACTION_IMAGE = "INTENT_ACTION_IMAGE";
    public static final String INTENT_ACTION_TEXT = "INTENT_ACTION_TEXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caption_design);

        initImages();
        setUpAdapter();

        mNextPage = findViewById(R.id.goto_caption);

        mEditorView = findViewById(R.id.photoEditorView);
        mEditorView.getSource().setScaleType(ImageView.ScaleType.FIT_XY);

        mWallpaper = findViewById(R.id.wallpaper);
        mTextEditingView = findViewById(R.id.text_editing);
        mText = findViewById(R.id.text);
        mFontSpinner = findViewById(R.id.font_family_spinner);

        mWallpaper.setTextColor(Color.BLACK);
        mText.setTextColor(Color.GRAY);
        Typeface robotoFont = ResourcesCompat.getFont(CaptionDesignActivity.this, R.font.roboto_medium);
        mPhotoEditor = new PhotoEditor.Builder(CaptionDesignActivity.this, mEditorView)
                .setPinchTextScalable(true)
                .setDefaultTextTypeface(robotoFont)
                .build();

        mPhotoEditor.addText(getIntent().getStringExtra(INTENT_CAPTION), Color.BLACK);
        initFontSpinner(mPhotoEditor);

        mWallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWallpaper.setTextColor(Color.BLACK);
                mText.setTextColor(Color.GRAY);
                mBackgroundImages.setVisibility(View.VISIBLE);
                mTextEditingView.setVisibility(View.GONE);
            }
        });
        mText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWallpaper.setTextColor(Color.GRAY);
                mText.setTextColor(Color.BLACK);
                mTextEditingView.setVisibility(View.VISIBLE);
                mBackgroundImages.setVisibility(View.GONE);
            }
        });

        mNextPage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(CaptionDesignActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    getPermissionToSaveImage();
                    return;

                }
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + ""
                        + System.currentTimeMillis() + ".png");
                try {
                    file.createNewFile();

                    SaveSettings saveSettings = new SaveSettings.Builder()
                            .setClearViewsEnabled(true)
                            .setTransparencyEnabled(true)
                            .build();

                    mPhotoEditor.saveAsFile(file.getAbsolutePath(), saveSettings, new PhotoEditor.OnSaveListener() {
                        @Override
                        public void onSuccess(@NonNull String imagePath) {
                            Log.d(TAG, "onSuccess: " + imagePath);
                            if (imagePath.equals("")) {
                                Log.d(TAG, "onSuccess: something went wrong");
                                return;
                            }
                            Intent intent = new Intent(CaptionDesignActivity.this, AddImagePostActivity.class);
                            intent.putExtra(INTENT_ACTION_TEXT, getIntent().getStringExtra(INTENT_CAPTION));
                            intent.putExtra(INTENT_ACTION_IMAGE, imagePath);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(CaptionDesignActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "onFailure: " + exception.getMessage());
                        }
                    });
                } catch (IOException e) {
                    Log.d(TAG, "onClick: " + e.getMessage());
                }
            }
        });


    }

    private void initFontSpinner(final PhotoEditor editor) {

        final String[] fontName = {
                "Roboto Black",
                "Roboto-BlackItalic",
                "Roboto-Bold",
                "Roboto-BoldItalic",
                "Roboto-Italic",
                "Roboto-Light",
                "Roboto-LightItalic",
                "Roboto-Medium",
                "Roboto-MediumItalic",
                "Roboto-Regular",
                "Roboto-Thin",
                "Roboto-ThinItalic"};
        final String[] fontFilePath = {
                "fonts/roboto_black.ttf",
                "fonts/roboto_black_italic.ttf",
                "fonts/roboto_bold.ttf",
                "fonts/roboto_bold_italic.ttf",
                "fonts/roboto_italic.ttf",
                "fonts/roboto_light.ttf",
                "fonts/roboto_light_italic.ttf",
                "fonts/roboto_medium.ttf",
                "fonts/roboto_medium_italic.ttf",
                "fonts/roboto_regular.ttf",
                "fonts/roboto_thin.ttf",
                "fonts/roboto_thin_italic.ttf"
        };
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, fontName);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFontSpinner.setAdapter(arrayAdapter);
        mFontSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Typeface typeface = Typeface.createFromAsset(getAssets(), fontFilePath[i]);
                String text = getIntent().getStringExtra(INTENT_CAPTION);
                editor.clearAllViews();
                editor.addText(typeface, text, Color.BLACK);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initImages() {
        CaptionImage image = new CaptionImage("https://images.unsplash.com/photo-1537412779515-d4d99ed35927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80");
        imageList.add(image);
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1tRx6hNiYZNGXlxmloKGQHYw6fYfXMaQP"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1Yz-rjZeZrbKtV9fnxnuhKs2EFL7iVTyC"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1-VJlFJ2ExiKFMrqfPxEncsZpsec6BNil"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1Kqf5yRy7qXNF9s5BkrtAS7QMXptdX0En"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1v4zERSivjQxlXnBZYVHEu4rxaYD1OGk5"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1c7thDcq4XhdKPGIAjMMJJinfOl2kr7IN"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1ejfJ3OviOgOng619Gs5adXPHYou0EEr4"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1IswfZWS4JOQnJf01tn_Tpu_-Lh-hA0ib"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1Yn9qzWMPConJ3T7UjBOia6J4dyjQ95_t"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1tDYix_LbQLbBJv2uGGZDB79q66xobiGO"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=12a8U60VhX9Pl03XczJb8JZ5n3dAuO6nG"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1QiwF1VhcZMuY2V3wc-A5wmZaEw9jZWFb"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1QPBCX39vI-5fuNsijbekNwyD-hIgH8vp"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1qGpeAzIrP--GVOPqI-9W4E51Gi9xVk5U"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1-BhKUHiyHCxmCcsFr-S7mXKh78JqX6uT"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1eW5I4gxKp3obqrH41wLJk6Su04mC2nk-"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1ZE0nNJBwDjxKIUOyBpwiMaQ8CJn6r4vm"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1Plgo8SaxyYkge5isPUFckYn6vOJGXU7y"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1oaCr5t3wngBsI8Sif3_UyKurj0hgwGiS"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1E8GghJmC28vzEg_5S7oDiPsAyT7BsMhQ"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=15Q-1AJNTZbYd-MYF6aRIEDUdDAeymgzp"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=155GUFy6RcD3U2TC6dj_W5EDlSl23_zRx"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1jT1sXD10zRwfRhww7u7xO8qyO9FvkTSH"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1eiQEExHpSDSMjFBg3pGZjYDZso8X2E-d"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1_MHGV-FcugMGnJMZALkCmd3vxzCkOJcC"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1UlNDrGETfCxe5Or84uD5uiMSbnLj3G0E"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1jFkyoXCahfxconpIYiGIRqoe7qGFrV_k"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1w61i1SV6vJ0XjYa2J3pBisj4S28FV1Lb"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1OMg85Jlfc80v6bkjwbYIQ5clP9xRQljT"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1_gaOAyLc5kfy--kv6JrSMeBJ8CFurAAN"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1r0vn_CkTfvzKsVBxLLNmIOOtbU2g7CTc"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1GP5EtnwCJQU57ifEg4zyY675G6t9uT2z"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1HEs2vr3J9pqMXxa1dP6ptogZVb97G_OX"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1THcjSSdZ0loZeuL6UuwMLjEzjp4_W1Oc"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1UHWayF4JdbcwTnlGaSZ8EsJNRWbALS1y"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1ad2RTx74kHFsX2dan6jDVTVSVykGZFKh"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1RCFWWjsATvpBsDzUEcV7UAz62ZvuAqla"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1vNy1tSCIogAzYRBUASxluK51nyJaW5NV"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1Hnlz0xtpXXyfHxpT1QiQ6mRTNP_XfxhR"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1DCQm_EUjpqDAqCKToS3Jv8mBzSymVb1v"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1UiNa3q4HPFe09mBmgufmm4jatyz0Ezzb"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1DDkzN2Y3_ReVr7tjdm4Ayxvn3gfrBDka"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=18UGZT9DiDk2T6j4fA7XJXq7IUe-Iz1QR"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1lxm8GHbIYoEfLDnTFEoM1RSqdImY-3kL"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1b9ouIan-s8ZmIGUsLeNyj7ZtHw0YRP5q"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1-B0QOF-xxYNicOPRUJ-cdL0dM2v3-xqF"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1JJrZ4vAvRf1Vkog0P-e8w93OpFNuXl-B"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1hBv5QXtWUbZR9K01-I48AgIbHDCk0cOq"));
        imageList.add(new CaptionImage("https://docs.google.com/uc?id=1NhUZ6iwDUU70U7oooAFDSRFCQyj3A2eh"));
    }

    private void setUpAdapter() {
        mBackgroundImages = findViewById(R.id.post_grid_images);
        adapter = new CaptionImageAdapter(this, imageList, this);
        mBackgroundImages.setLayoutManager(new GridLayoutManager(this, 3));
        mBackgroundImages.setAdapter(adapter);
    }

    @Override
    public void onImageClick(CaptionImage image, int i) {
        if (i == 0) {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");

            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");

            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

            startActivityForResult(chooserIntent, PICK_IMAGE);
            return;
        }
        Glide.with(this)
                .load(image.getCaptionImage())
                .placeholder(R.drawable.placeholder)
                .into(mEditorView.getSource());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Uri image = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), image);
                        Bitmap markedImage = addWatermark(getResources(), bitmap);
                        Glide.with(this)
                                .load(markedImage)
                                .placeholder(R.drawable.placeholder)
                                .into(mEditorView.getSource());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getPermissionToSaveImage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, SAVE_IMAGE_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == SAVE_IMAGE_REQUEST_CODE) {
            if (grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: granted");
            } else {
                Log.d(TAG, "onRequestPermissionsResult: denied");
                finishAffinity();
            }
        }
    }


    /**
     * Adds a watermark on the given image.
     */
    public static Bitmap addWatermark(Resources res, Bitmap source) {
        int w, h;
        Canvas c;
        Paint paint;
        Bitmap bmp, watermark;
        Matrix matrix;
        float scaleHeight, scaleWidth;
        RectF r;
        w = source.getWidth();
        h = source.getHeight();
        // Create the new bitmap
        bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
        // Copy the original bitmap into the new one
        c = new Canvas(bmp);
        c.drawBitmap(source, 0, 0, paint);
        // Load the watermark
        watermark = BitmapFactory.decodeResource(res, R.mipmap.logo);
        // Scale the watermark to be approximately 40% of the source image height
        scaleHeight = (float) (((float) h * 0.30) / (float) watermark.getHeight());
        scaleWidth = (float) (((float) h * 0.30) / (float) watermark.getWidth());
        // Create the matrix
        matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // Determine the post-scaled size of the watermark
        r = new RectF(0, 0, watermark.getWidth(), watermark.getHeight());
        matrix.mapRect(r);
        // Move the watermark to the bottom right corner
        matrix.postTranslate(w - r.width(), h - r.height());
        // Draw the watermark
        c.drawBitmap(watermark, matrix, paint);
        // Free up the bitmap memory
        watermark.recycle();
        return bmp;
    }
}