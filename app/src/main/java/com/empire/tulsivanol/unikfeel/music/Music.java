package com.empire.tulsivanol.unikfeel.music;

public class Music {
    private String musicImage,musicName;
    private String musicUrl;
    public Music(){}

    public Music(String musicImage, String musicName,String musicUrl) {
        this.musicImage = musicImage;
        this.musicName = musicName;
        this.musicUrl = musicUrl;
    }

    public String getMusicImage() {
        return musicImage;
    }

    public void setMusicImage(String musicImage) {
        this.musicImage = musicImage;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }
}
