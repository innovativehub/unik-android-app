package com.empire.tulsivanol.unikfeel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.Model.User;
import com.empire.tulsivanol.unikfeel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    private Switch mTagMeSwitch, mPostNotificationSwitch, mOldPostNotificationSwitch;
    private MaterialTextView mWebProfile, mDeactivateAccount, mBlockUser;
    private DatabaseReference mUserReference;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        mUserReference = FirebaseDatabase.getInstance().getReference("Users");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mTagMeSwitch = findViewById(R.id.setting_switch_tag_me);
        mPostNotificationSwitch = findViewById(R.id.setting_switch_notification_sound);
        mOldPostNotificationSwitch = findViewById(R.id.setting_switch_old_post_notification);

        mWebProfile = findViewById(R.id.route_web_profile);
        mDeactivateAccount = findViewById(R.id.route_deactivate_account);
        mBlockUser = findViewById(R.id.route_block_user);

        getUserInfo();

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                finish();
            }
        });

        mTagMeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(SettingsActivity.this, "Tag me", Toast.LENGTH_SHORT).show();
            }
        });
        mPostNotificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                turnOnOffNotification(b);
//                Toast.makeText(SettingsActivity.this, "Notification ", Toast.LENGTH_SHORT).show();
            }
        });
        mOldPostNotificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(SettingsActivity.this, "Old Post Notification Sound", Toast.LENGTH_SHORT).show();
            }
        });

        mWebProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingsActivity.this, "Web Profile", Toast.LENGTH_SHORT).show();
            }
        });
        mDeactivateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingsActivity.this, "Deactivate Account", Toast.LENGTH_SHORT).show();
            }
        });
        mBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingsActivity.this, "Block User", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getUserInfo() {
        mUserReference.child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                mPostNotificationSwitch.setChecked(user.getNotificationStatus());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void turnOnOffNotification(final Boolean value) {
//        mUserReference.child(firebaseUser.getUid()).child("notificationStatus").setValue(false);
        mUserReference.child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("notificationStatus", value);
                mUserReference.child(firebaseUser.getUid()).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                        }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(SettingsActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}