package com.empire.tulsivanol.unikfeel.music;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.empire.tulsivanol.unikfeel.R;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MusicViewHolder>{

    private List<Music> musicList;
    private Context mContext;
    private static final String TAG = "MusicAdapter";
    private MusicClickListener musicClickListener;
    private int index = -1;

    public MusicAdapter(Context mContext,List<Music> musicList,MusicClickListener musicClickListener) {
        this.musicList = musicList;
        this.mContext = mContext;
        this.musicClickListener = musicClickListener;
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_music,parent,false);
        return new MusicViewHolder(view,musicClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MusicViewHolder holder, int position) {
        final Music music = musicList.get(position);
        holder.mSongImage.setImageResource(R.drawable.music_icon);
        holder.mSongName.setText(music.getMusicName());

        if (index == position){
            holder.itemView.setBackgroundColor(Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    class MusicViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView mSongImage;
        private TextView mSongName;
        private ImageView mSongPlayBtn;
        private MusicClickListener musicClickListener;

        public MusicViewHolder(@NonNull View itemView,MusicClickListener musicClickListener) {
            super(itemView);
            this.musicClickListener = musicClickListener;

            mSongImage = itemView.findViewById(R.id.music_image);
            mSongName = itemView.findViewById(R.id.music_name);
            mSongPlayBtn = itemView.findViewById(R.id.music_btn_play);

            mSongPlayBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemView.setBackgroundColor(Color.GRAY);
            index = getAdapterPosition();
            musicClickListener.onMusicClickListener(musicList.get(getAdapterPosition()));
        }
    }

    public interface MusicClickListener {
        void onMusicClickListener(Music music);
    }
}
