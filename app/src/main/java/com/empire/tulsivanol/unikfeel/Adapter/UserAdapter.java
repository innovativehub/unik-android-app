package com.empire.tulsivanol.unikfeel.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.empire.tulsivanol.unikfeel.Fragments.ProfileFragment;
import com.empire.tulsivanol.unikfeel.Model.User;
import com.empire.tulsivanol.unikfeel.R;
import com.empire.tulsivanol.unikfeel.activities.MainActivity;
import com.empire.tulsivanol.unikfeel.api.ApiService;
import com.empire.tulsivanol.unikfeel.api.Client;
import com.empire.tulsivanol.unikfeel.api.Data;
import com.empire.tulsivanol.unikfeel.api.MyResponse;
import com.empire.tulsivanol.unikfeel.api.Sender;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ImageViewHolder> {

    private Context mContext;
    private List<User> mUsers;
    private boolean isFragment;
    public String mCurrentState;
    private DatabaseReference mRootReference;

    String user_id;


    private FirebaseUser mFirebaseUser;
    private ApiService apiService;


    public UserAdapter(Context context, List<User> users, boolean isFragment) {
        mContext = context;
        mUsers = users;
        this.isFragment = isFragment;
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mRootReference = FirebaseDatabase.getInstance().getReference("Follow");
    }

    @NonNull
    @Override
    public UserAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        apiService = Client.getClient("https://fcm.googleapis.com/").create(ApiService.class);
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_item, parent, false);
        return new UserAdapter.ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserAdapter.ImageViewHolder holder, final int position) {

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mCurrentState = "not_friends";

        final User user = mUsers.get(position);

        holder.btn_follow.setVisibility(View.VISIBLE);
        isFollowing(user.getId(), holder.btn_follow);

        holder.username.setText(user.getUsername());
        holder.fullname.setText(user.getFullname());
        Glide.with(mContext).load(user.getImageurl()).into(holder.image_profile);

        if (user.getId().equals(mFirebaseUser.getUid())) {
            holder.btn_follow.setVisibility(View.INVISIBLE);
            holder.itemView.setEnabled(false);
            holder.itemView.setClickable(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFragment) {
                    SharedPreferences.Editor editor = mContext.getSharedPreferences("PREFS", MODE_PRIVATE).edit();
                    editor.putString("profileid", user.getId());
                    editor.putString("type", "user");
                    editor.apply();

                    ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new ProfileFragment()).commit();
                } else {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.putExtra("publisherid", user.getId());
                    mContext.startActivity(intent);
                }
            }
        });

//        if (CURRENT_STATE.equals("NOT_FRIEND")) {
//            holder.btn_follow.setText("Follow");
//        }
//
//        if (CURRENT_STATE.equals("REQUEST_SENT")) {
//            holder.btn_follow.setText("Requested");
//        }

        holder.btn_follow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {

                if (holder.btn_follow.getText().toString().equals("follow")) {
                    //i am (current user)
                    mRootReference.child(mFirebaseUser.getUid())
                            // following
                            .child("following").
                            //this user (user.getId)
                                    child(user.getId()).setValue(false);


                    mRootReference.child(user.getId())
                            .child("followers").child(mFirebaseUser.getUid()).setValue(false);
                    holder.btn_follow.setText("requested");

                    mCurrentState = "requested";
                    addNotification(FirebaseInstanceId.getInstance().getToken());
                    sendNotifications(FirebaseInstanceId.getInstance().getToken(),"New Notification",user.getFullname()+" "+"wants to follow you");
                } else if (holder.btn_follow.getText().toString().equals("requested")) {
                    mRootReference.child(mFirebaseUser.getUid())
                            .child("following").child(user.getId()).removeValue();
                    mRootReference.child(user.getId())
                            .child("followers").child(mFirebaseUser.getUid()).removeValue();
                    holder.btn_follow.setText("follow");
                } else if (holder.btn_follow.getText().toString().equals("following")) {
                    mRootReference.child(mFirebaseUser.getUid())
                            .child("following").child(user.getId()).removeValue();
                    mRootReference.child(user.getId())
                            .child("followers").child(mFirebaseUser.getUid()).removeValue();
                }

                mRootReference.child(mFirebaseUser.getUid())
                        .child("following")
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (!dataSnapshot.exists()) {
                                    holder.btn_follow.setText("Follow");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }

        });
    }

    public void sendNotifications(String usertoken, String title, String message) {
        Data data = new Data(null,message,title,0,null);
        Sender sender = new Sender(data, usertoken);
        apiService.sendNotification(sender)
                .enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                        if (response.code() == 200) {
                            if (response.isSuccessful()) {
                                Log.d("this", "onResponse: send");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MyResponse> call, Throwable t) {
                        Log.d("this", "onFailure: ", t);
                    }
                });
    }

    private void addNotification(String userid) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(userid);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String dateString = dateFormat.format(date).toString();
        String notificationId = reference.push().getKey();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("userid", mFirebaseUser.getUid());
        hashMap.put("notificationId", notificationId);
        hashMap.put("text", "wants to follow you");
        hashMap.put("postid", "");
        hashMap.put("time", dateString);
        hashMap.put("ispost", false);

        reference.child(notificationId).setValue(hashMap);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public TextView username;
        public TextView fullname;
        public CircleImageView image_profile;
        public Button btn_follow;

        public ImageViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            fullname = itemView.findViewById(R.id.fullname);
            image_profile = itemView.findViewById(R.id.image_profile);
            btn_follow = itemView.findViewById(R.id.btn_follow);
        }
    }

    private void isFollowing(final String userid, final Button button) {

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference reference = mRootReference
                // i am
                .child(firebaseUser.getUid())
                //following
                .child("following");
        // this person with userId
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(userid).exists() && dataSnapshot.child(userid).getValue().equals(true)) {
                    button.setText("following");
                } else if (dataSnapshot.child(userid).exists() && dataSnapshot.child(userid).getValue().equals(false)) {
                    button.setText("requested");
                } else {
                    button.setText("follow");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public ProfileFragment newInstance(String param1) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString("ARG_PARAM1", param1);
        fragment.setArguments(args);
        return fragment;
    }
}