package com.empire.tulsivanol.unikfeel.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.empire.tulsivanol.unikfeel.Adapter.NotificationAdapter;
import com.empire.tulsivanol.unikfeel.Model.Notification;
import com.empire.tulsivanol.unikfeel.Model.User;
import com.empire.tulsivanol.unikfeel.R;
import com.empire.tulsivanol.unikfeel.api.ApiService;
import com.empire.tulsivanol.unikfeel.api.Client;
import com.empire.tulsivanol.unikfeel.api.Data;
import com.empire.tulsivanol.unikfeel.api.MyResponse;
import com.empire.tulsivanol.unikfeel.api.Sender;
import com.empire.tulsivanol.unikfeel.api.Token;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationFragment extends Fragment {

    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private List<Notification> notificationList;
    final private String FCM_API = "https://fcm.googleapis.com/fcm/send";
    final private String serverKey = "key=" + R.string.server_key;
    final private String contentType = "application/json";
    private ApiService apiService;
    boolean notify = true;
    private FirebaseUser firebaseUser;

    private static final String TAG = "NotificationFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        apiService = Client.getClient("https://fcm.googleapis.com/").create(ApiService.class);


        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        notificationList = new ArrayList<>();
        notificationAdapter = new NotificationAdapter(getContext(), notificationList);
        recyclerView.setAdapter(notificationAdapter);

        readNotifications();
        return view;
    }

    private void readNotifications() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                notificationList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    final Notification notification = snapshot.getValue(Notification.class);
                    final String msg = notification.getText();
                    notificationList.add(notification);
                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(notification.getUserid());
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            final User user = dataSnapshot.getValue(User.class);
                            if (notify) {
                                Log.d("UserId", "onDataChange: " + user.getId());
                                FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        User user1 = dataSnapshot.getValue(User.class);
                                        if (user1.getNotificationStatus()) {
                                            sendNotification(user.getId(), user.getUsername(), msg, notification.getUserid());
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                            notify = false;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

                Collections.reverse(notificationList);
                notificationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void sendNotification(String receiver, final String username, final String msg, final String userId) {
        Log.d(TAG, "sendNotification: " + receiver);
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Token");
        Query query = databaseReference.orderByKey().equalTo(userId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Token token = ds.getValue(Token.class);
                    Log.d(TAG, "onDataChange: " + dataSnapshot.toString());
                    Data data = new Data(firebaseUser.getUid(), username + " " + msg, "New Notification", R.mipmap.logo, userId);
                    Log.d(TAG, "onDataChange: " + token.getToken());
                    Log.d("GETTOKEN", "onDataChange: " + data.toString() + "token " + token.getToken());
                    Sender sender = new Sender(data, token.getToken());
                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if (response.code() == 200) {
                                        if (response.isSuccessful()) {
                                            Log.d(TAG, "onResponse: send");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {
                                    Log.d(TAG, "onFailure: ", t);
                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
