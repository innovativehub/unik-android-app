package com.empire.tulsivanol.unikfeel.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;

public class PostAudioVideoActivity extends AppCompatActivity {

    private static final int RECORD_AUDIO_REQUEST_CODE = 123;
    private static final int REQUEST_AUDIO_SERVICE = 124;
    int requestCode = 0;
    private MaterialTextView mBackBtn;
    private TextInputEditText mPostCaption;
    private ImageView mNextBtn, mBlogPostBtn, mAudioPostBtn, mVideoPostBtn;

    private String fileName = null;

    public static final int VIDEO_CAPTURE = 101;

    private DatabaseReference mDatabase;
    private StorageTask<UploadTask.TaskSnapshot> uploadTask;
    StorageReference storageRef;
    private FirebaseUser currentUser;

    private static final String TAG = "PostAudioVideoActivity";
    public static final String INTENT_CAPTION = "postCaption";
    private MaterialTextView mMaxLimit;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_audio_video);

        mDatabase = FirebaseDatabase.getInstance().getReference("Posts");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();


        mBackBtn = findViewById(R.id.back);
        mNextBtn = findViewById(R.id.next);
        mBlogPostBtn = findViewById(R.id.blog_post);
        mAudioPostBtn = findViewById(R.id.blog_audio);
        mVideoPostBtn = findViewById(R.id.blog_video);
        mPostCaption = findViewById(R.id.post_text);
        mMaxLimit = findViewById(R.id.max_limit);

//        get size of text
        mPostCaption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = charSequence.length();
                String text = String.valueOf(1250 - length);
                mMaxLimit.setText(text + " left");
                if (charSequence.length() > 1250) {
                    Toast.makeText(PostAudioVideoActivity.this, "Limit crossed", Toast.LENGTH_SHORT).show();
                    mNextBtn.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getPermissionToRecordAudio();
        if (!hasCamera()) {
            mVideoPostBtn.setVisibility(View.GONE);
        }

        mBlogPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PostAudioVideoActivity.this, BlogCategoryActivity.class));
            }
        });

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PostAudioVideoActivity.this, MainActivity.class));
                finish();
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String caption = mPostCaption.getText().toString().trim();
                Intent intent = new Intent(PostAudioVideoActivity.this, CaptionDesignActivity.class);
                intent.putExtra(INTENT_CAPTION, caption);
                startActivity(intent);
            }
        });

        mVideoPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 600);
                startActivityForResult(intent, VIDEO_CAPTURE);
            }
        });

        mAudioPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/UnikFeel/Audio");
                try {
                    if (dir.exists()) {
                        System.out.println("Directory already exist");
                    } else {
                        dir.mkdirs();
                    }
                    if (dir.mkdir()) {
                        System.out.println("Directory created");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int color = getResources().getColor(R.color.colorPrimary);
                fileName = dir + "/recorded_" + System.currentTimeMillis() + ".mp3";
//                filePath = Environment.getExternalStorageDirectory() + "/recorded_audio.wav";
                AndroidAudioRecorder.with(PostAudioVideoActivity.this)
                        .setColor(color)
                        .setFilePath(fileName)
                        .setRequestCode(REQUEST_AUDIO_SERVICE)
                        .setSource(AudioSource.MIC)
                        .setChannel(AudioChannel.STEREO)
                        .setSampleRate(AudioSampleRate.HZ_48000)
                        .setAutoStart(false)
                        .setKeepDisplayOn(true)
                        .record();
            }
        });

    }

    private boolean hasCamera() {
        return (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getPermissionToRecordAudio() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, RECORD_AUDIO_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onRequestPermissionsResult: granted");
            } else {
                Log.d(TAG, "onRequestPermissionsResult: denied");
                finishAffinity();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {

                File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/UnikFeel/Video");
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int maxX = size.x;
                int maxY = size.y;

                String inputVideoPath = "";
                if (data != null) {
//                    inputVideoPath = data.getData().toString();
//
//                    Log.d("INPUTVIDEOPATH", "onActivityResult: "+inputVideoPath);
//                    String watermarkPath = resourceToUri(this, R.mipmap.logo).toString();
//                    String outputVideoPath = dir + "/recorded_" + System.currentTimeMillis() + ".mp4";
//                    String waterMarkX = String.valueOf(maxX);
//                    String waterMarkY = String.valueOf(maxY);
//
//
//                    try {
//                        waterMarkHelper = new WaterMarkHelper(inputVideoPath,
//                                watermarkPath, outputVideoPath, waterMarkX, waterMarkY);
//                        waterMarkHelper.addVideoWaterMark(this);
//                    } catch (CommandValidationException e) {
//                        e.printStackTrace();
//                    }

                    startActivity(new Intent(PostAudioVideoActivity.this,VideoEditingActivity.class)
                    .putExtra("videoname",data.getData().toString()));

                }
//                    File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/videos");
//                    if (f.mkdirs() || f.isDirectory()) {
//                        new VideoCompressAsyncTask(PostAudioVideoActivity.this).execute(data.getData().toString(), f.getPath());
//
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Video Recording has been cancelled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Video recording failed", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_AUDIO_SERVICE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: " + fileName);

                if (fileName == null) {
                    Log.d(TAG, "onActivityResult: file size error");
                    return;
                }
                Uri audioUri = Uri.fromFile(new File(fileName));
                Intent intent = new Intent(PostAudioVideoActivity.this,AudioMergingActivity.class);
                intent.putExtra("tulsivanol",String.valueOf(audioUri));
                startActivity(intent);
//                final ProgressDialog pd = new ProgressDialog(this);
//                pd.setCanceledOnTouchOutside(false);
//                pd.setMessage("Uploading  Audio");
//                pd.show();
//                final Uri audioUri = Uri.fromFile(new File(fileName));
//                Log.d("AUDIOURI", "onActivityResult: " + audioUri);
//                final StorageReference storageReference = FirebaseStorage.getInstance().getReference("audios");
//                storageReference.child(System.currentTimeMillis() + currentUser.getUid())
//                        .putFile(audioUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//                        if (task.isSuccessful()) {
//                            task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
//                                @Override
//                                public void onComplete(@NonNull Task<Uri> task) {
//                                    if (task.isSuccessful()) {
//                                        Uri downloadUri = task.getResult();
//                                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//                                        Date date = new Date();
//                                        String dateString = dateFormat.format(date).toString();
//                                        String postId = mDatabase.push().getKey();
//                                        HashMap<String, Object> hashMap = new HashMap<>();
//                                        hashMap.put("postid", postId);
//                                        hashMap.put("postimage", downloadUri.toString());
//                                        hashMap.put("posttype", "audio");
//                                        hashMap.put("postedDate", dateString);
//                                        hashMap.put("description", audioUri.getLastPathSegment());
//                                        hashMap.put("publisher", currentUser.getUid());
//                                        mDatabase.child(postId).setValue(hashMap);
//
//                                        pd.dismiss();
//                                        Toast.makeText(PostAudioVideoActivity.this, "Audio Posted successfully", Toast.LENGTH_SHORT).show();
//                                        startActivity(new Intent(PostAudioVideoActivity.this, MainActivity.class));
//                                        finish();
//                                    } else {
//                                        pd.dismiss();
//                                        Toast.makeText(PostAudioVideoActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                        } else {
//                            pd.hide();
//                            Toast.makeText(PostAudioVideoActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//                Intent intent = new Intent(PostAudioVideoActivity.this, BackgroundMusicActivity.class);
//                intent.putExtra("audioUri", fileName);
//                startActivity(intent);

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Recording has been canceled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d(TAG, "onActivityResult: error");
        }
    }

    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }
}