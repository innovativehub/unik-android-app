package com.empire.tulsivanol.unikfeel.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.empire.tulsivanol.unikfeel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class PhoneVerifyActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private MaterialButton mSubmitBtn;
    private DatabaseReference reference;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    private String email, password, username, fullname, mobile_number, referral_code;
    private MaterialEditText mOtpCode;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    ProgressDialog pd;
    private String verificationId = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private static final String TAG = "PhoneVerifyActivity";
    private MaterialTextView mShowMessage;
    private String token = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verify);


        auth = FirebaseAuth.getInstance();
        mSubmitBtn = findViewById(R.id.phone_submit_btn);
        mShowMessage = findViewById(R.id.dummy);
        mOtpCode = findViewById(R.id.phone_otp_code);
        progressBar = findViewById(R.id.progress_circular);
        pd = new ProgressDialog(PhoneVerifyActivity.this);
        pd.setMessage("Please wait...");
        pd.setCanceledOnTouchOutside(false);

        Intent i = getIntent();
        email = i.getStringExtra("email");
        password = i.getStringExtra("password");
        username = i.getStringExtra("username");
        fullname = i.getStringExtra("fullname");
        mobile_number = i.getStringExtra("mobile_number");
        referral_code = i.getStringExtra("referral_code");
        progressBar.setVisibility(View.VISIBLE);
        mShowMessage.setText("Otp has been send to the " + mobile_number);
        Log.d(TAG, "onCreate: " + email + password + username + fullname + mobile_number + referral_code);
        sendVerificationCode(mobile_number);
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = mOtpCode.getText().toString().trim();
//                registerUser();
                if (code.isEmpty() || code.length() < 6) {
                    mOtpCode.setError("Enter valid code");
                    mOtpCode.requestFocus();
                    return;
                }
                pd.show();
                verifyVerificationCode(code);
            }
        });
    }

    private void sendVerificationCode(String mobile) {
        Log.d(TAG, "sendVerificationCode: " + mobile);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + mobile,
                120,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                callbacks);
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            Log.d(TAG, "onVerificationCompleted: " + phoneAuthCredential);
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                mOtpCode.setText(code);
                verifyVerificationCode(code);
//                registerUser()
            }
            pd.hide();
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            pd.dismiss();
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                Toast.makeText(PhoneVerifyActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            } else if (e instanceof FirebaseTooManyRequestsException) {
                Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                        Snackbar.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
            mResendToken = forceResendingToken;
        }
    };


    private void verifyVerificationCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        Log.d(TAG, "verifyVerificationCode: " + credential);
        progressBar.setVisibility(View.GONE);
        registerUser();

    }

    private void registerUser() {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(PhoneVerifyActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            pd.dismiss();
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            String userID = firebaseUser.getUid();

                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (task.isSuccessful()) {
                                        token = task.getResult().getToken();
                                    }
                                }
                            });

                            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("id", userID);
                            map.put("username", username.toLowerCase());
                            map.put("fullname", fullname);
                            map.put("mobile_number", mobile_number);
                            map.put("referral_code", referral_code);
                            map.put("imageurl", "https://storage.googleapis.com/stateless-campfire-pictures/2019/05/e4629f8e-defaultuserimage-15579880664l8pc.jpg");
                            map.put("bio", "");
                            map.put("link", "");
                            map.put("token", token);
                            map.put("notificationStatus", true);

                            reference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(PhoneVerifyActivity.this, "Your account has been created", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(PhoneVerifyActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(PhoneVerifyActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            pd.hide();
                            Toast.makeText(PhoneVerifyActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}